const jwt = require('jsonwebtoken');

function checkAuth(req, res, next) {
    jwt.verify(req.headers.authorization, 'secret-key', (err, decoded) => {
        if(error) {
            res.status(401).send({error, message: 'Incorrect authorization token'});
        } else {
            req.decoded = decoded;
            next();
        }
    });
}
