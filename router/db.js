const express = require('express');
const path = require('path');

const DbController = require('../controllers/db-controller');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: (req, file, cb) => { cb(null, 'images'); },
    filename: (req, file, cb) => { cb(null, file.originalname); }
});
const upload = multer({storage});

class DbRouter {
    constructor() {
        this.router = express.Router();
        this.controller = new DbController();
        this.routes();
    }

    routes() {
        this.router.post('/', upload.single('picture'), this._saveItem.bind(this));
        this.router.put('/:id/count', this.isAdmin, this.isAdminNext, this._count);
    }

    _saveItem(req, res) {
        console.log(req.file);
        res.json({});
        // const data = {...req.body, filename: req.file.filename};
        // this.controller.saveItem(data)
        //     .then(response => res.send(response))
        //     .catch(err => res.status(500).send(err));
    }

    _count(req, res) {
        res.send(req.isAdmin);
    }

    isAdmin(req, res, next) {
        if(req.headers.authorization === undefined || req.headers.authorization !== 'aaaa') {
            res.status(403).send('Privilages');
        } else {
            req.isAdmin = true;
            next();
        }
    }
    isAdminNext(req, res, next) {
        if(req.isAdmin) {
            res.send('isADMIN I am sure')
        }else {
            res.status(401);
        }
    }
}

module.exports = DbRouter;
