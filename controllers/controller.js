const PouchDB = require('pouchdb');
PouchDB.plugin(require('pouchdb-find'));

class Controller {
    constructor(dbName) {
        this.db = new PouchDB(`../db/${dbName}`);
    }
}

module.exports = Controller;
