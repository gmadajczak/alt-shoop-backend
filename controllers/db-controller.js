const Controller = require('./controller');

class DbController extends Controller {
    constructor() {
        super('shop-backend');
    }

    saveItem(data) {
        return this.db.post(data);
    }

    getById(id) {
        return this.db.get(id);
    }

    getAll() {
        return this.db.allDocs({include_docs: true});
    }
}

module.exports = DbController;
